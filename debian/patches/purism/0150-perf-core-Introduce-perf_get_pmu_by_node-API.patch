From: Leonard Crestez <leonard.crestez@nxp.com>
Date: Tue, 12 Nov 2019 23:39:05 +0200
Subject: perf/core: Introduce perf_get_pmu_by_node API

Add a new public API to fetch a pointer to struct pmu from a devicetree
node devicetree node. This is meant to be used by other drivers to
create in-kernel counter for custom hardware PMUs with automatically
allocated pmu->type.

This is implementated by adding a new "parent_dev" field which pmu
drivers can optionally fill. This parent device is set as the parent of
the pmu->dev created on the pmu_bus. This also has the nice side-effect
of creating additional symlinks inside sysfs.

The actual per_get_pmu_by_node function is implemented through
bus_find_device and callers are asked to "put_device(pmu->dev)" when
done. This might cause problems if pmu_bus is initialize late but
consumers should handle errors by returning -EPROBE_DEFER anyway.

Signed-off-by: Leonard Crestez <leonard.crestez@nxp.com>
---
 drivers/perf/arm_pmu_platform.c  |  1 +
 drivers/perf/fsl_imx8_ddr_perf.c |  1 +
 include/linux/perf_event.h       |  8 ++++++++
 kernel/events/core.c             | 24 ++++++++++++++++++++++++
 4 files changed, 34 insertions(+)

diff --git a/drivers/perf/arm_pmu_platform.c b/drivers/perf/arm_pmu_platform.c
index 3596db3..6d7028c 100644
--- a/drivers/perf/arm_pmu_platform.c
+++ b/drivers/perf/arm_pmu_platform.c
@@ -197,6 +197,7 @@ int arm_pmu_device_probe(struct platform_device *pdev,
 		return -ENOMEM;
 
 	pmu->plat_device = pdev;
+	pmu->pmu.parent = &pdev->dev;
 
 	ret = pmu_parse_irqs(pmu);
 	if (ret)
diff --git a/drivers/perf/fsl_imx8_ddr_perf.c b/drivers/perf/fsl_imx8_ddr_perf.c
index 92611c9..d6d6f3f 100644
--- a/drivers/perf/fsl_imx8_ddr_perf.c
+++ b/drivers/perf/fsl_imx8_ddr_perf.c
@@ -765,6 +765,7 @@ static int ddr_perf_probe(struct platform_device *pdev)
 		goto ddr_perf_err;
 	}
 
+	pmu->pmu.parent = &pdev->dev;
 	ret = perf_pmu_register(&pmu->pmu, name, -1);
 	if (ret)
 		goto ddr_perf_err;
diff --git a/include/linux/perf_event.h b/include/linux/perf_event.h
index fcb834d..47ab7df 100644
--- a/include/linux/perf_event.h
+++ b/include/linux/perf_event.h
@@ -1080,6 +1080,14 @@ extern void perf_event_itrace_started(struct perf_event *event);
 extern int perf_pmu_register(struct pmu *pmu, const char *name, int type);
 extern void perf_pmu_unregister(struct pmu *pmu);
 
+#ifdef CONFIG_OF
+extern struct pmu* perf_get_pmu_by_node(struct device_node *node);
+#else
+static inline struct pmu* perf_get_pmu_by_node(struct device_node *node) {
+	return NULL;
+}
+#endif
+
 extern void __perf_event_task_sched_in(struct task_struct *prev,
 				       struct task_struct *task);
 extern void __perf_event_task_sched_out(struct task_struct *prev,
diff --git a/kernel/events/core.c b/kernel/events/core.c
index 4f6b18e..0ec462b 100644
--- a/kernel/events/core.c
+++ b/kernel/events/core.c
@@ -11553,6 +11553,30 @@ static int pmu_dev_alloc(struct pmu *pmu)
 	goto out;
 }
 
+#ifdef CONFIG_OF
+static int perf_match_parent_of_node(struct device *dev, const void *np)
+{
+       return dev->parent && dev->parent->of_node == np;
+}
+
+/**
+ * perf_get_pmu_by_node() - Fetch PMU instance via devicetree node
+ *
+ * @node: devicetree node for PMU
+ *
+ * Caller should put_device(pmu-dev) when done.
+ */
+struct pmu* perf_get_pmu_by_node(struct device_node *node)
+{
+       struct device* dev;
+
+       dev = bus_find_device(&pmu_bus, NULL, node, perf_match_parent_of_node);
+
+       return dev ? dev_get_drvdata(dev) : NULL;
+}
+EXPORT_SYMBOL_GPL(perf_get_pmu_by_node);
+#endif
+
 static struct lock_class_key cpuctx_mutex;
 static struct lock_class_key cpuctx_lock;
 
