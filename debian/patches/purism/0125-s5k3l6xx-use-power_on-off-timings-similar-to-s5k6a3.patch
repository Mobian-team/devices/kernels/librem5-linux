From: Martin Kepplinger <martin.kepplinger@puri.sm>
Date: Mon, 27 Mar 2023 11:56:08 +0200
Subject: s5k3l6xx: use power_on/off timings similar to s5k6a3

On one, but not all Evergreen device I see power_on() failing, resulting in

[    8.132813] s5k3l6xx 3-002d: using default 24000000 Hz clock frequency
[    8.132837] s5k3l6xx 3-002d: GPIO lookup for consumer rstn
[    8.132843] s5k3l6xx 3-002d: using device tree for GPIO lookup
[    8.132931] s5k3l6xx 3-002d: probe i2c ffff0000010e4800
[    8.200983] s5k3l6xx: i2c_read: error during transfer (-6)

or

[    7.409464] s5k3l6xx 3-002d: using default 24000000 Hz clock frequency
[    7.409486] s5k3l6xx 3-002d: using DT '/soc@0/bus@30800000/i2c@30a50000/camera@2d' for 'rstn' GPIO lookup
[    7.410335] s5k3l6xx 3-002d: probe i2c ffff000001522000
[    7.458606] s5k3l6xx 3-002d: model low: 0x30
[    7.459177] s5k3l6xx 3-002d: model high: 0xC6
[    7.459701] s5k3l6xx 3-002d: revision mismatch: 0x0 != 0xB0
[    7.488683] s5k3l6xx: probe of 3-002d failed with error -22

Using a different power_on() sequence inspired by other Samsung sensor
drivers like s5k6a3, makes things work for my devices.
---
 drivers/media/i2c/s5k3l6xx.c | 20 +++++++++++++++++---
 1 file changed, 17 insertions(+), 3 deletions(-)

diff --git a/drivers/media/i2c/s5k3l6xx.c b/drivers/media/i2c/s5k3l6xx.c
index de4230c..f23f97a 100644
--- a/drivers/media/i2c/s5k3l6xx.c
+++ b/drivers/media/i2c/s5k3l6xx.c
@@ -564,6 +564,8 @@ static int s5k3l6xx_power_on(struct s5k3l6xx *state)
 	if (ret < 0)
 		goto err;
 
+	usleep_range(10, 20);
+
 	ret = clk_set_rate(state->clock, state->mclk_frequency);
 	if (ret < 0)
 		goto err_reg_dis;
@@ -575,9 +577,21 @@ static int s5k3l6xx_power_on(struct s5k3l6xx *state)
 	v4l2_dbg(1, debug, &state->sd, "ON. clock frequency: %ld\n",
 		 clk_get_rate(state->clock));
 
-	usleep_range(500, 1000);
+	/* 1ms = 25000 cycles at 25MHz */
+	usleep_range(1000, 1500);
 	gpiod_set_value_cansleep(state->rst_gpio, 0);
-	usleep_range(500, 1000);
+
+	/*
+	 * this additional reset-active is not documented but makes power-on
+	 * more stable.
+	 */
+	usleep_range(400, 800);
+	gpiod_set_value_cansleep(state->rst_gpio, 1);
+	usleep_range(400, 800);
+	gpiod_set_value_cansleep(state->rst_gpio, 0);
+
+	/* t4+t5 delays (depending on coarse integration time) + safety margin */
+	msleep(10);
 
 	return 0;
 
@@ -598,7 +612,7 @@ static int s5k3l6xx_power_off(struct s5k3l6xx *state)
 	state->apply_cfg = 0;
 	state->apply_crop = 0;
 
-	usleep_range(500, 1000);
+	usleep_range(20, 40);
 
 	gpiod_set_value_cansleep(state->rst_gpio, 1);
 
